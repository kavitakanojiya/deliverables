Rails.application.routes.draw do
  devise_for :users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)

  devise_scope :user do
    post '/users/sign_in', to: 'devise_sessions#create'
    get '/users/sign_in', to: 'devise_sessions#new'
    delete '/users/sign_out', to: 'devise_sessions#destroy'
  end

  resources :orders, only: [:index]
  resources :order_references, only: [:index] do
    collection do
      get :todays
    end
  end

  root to: 'home#index'
end
