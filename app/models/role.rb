class Role < ApplicationRecord
  LIST = ['admin', 'carrier', 'shopper','supervisor', 'vendor']

  class << self
    Role::LIST.each do |role|
      define_method role do
        by_role(role)
      end
    end

    def by_role(role)
      self.find_by(name: role)
    end
  end
end
