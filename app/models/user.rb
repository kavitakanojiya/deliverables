class User < ApplicationRecord
  include Accessible
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, 
    :recoverable, :rememberable, :trackable, :validatable                                                  

  belongs_to :role

  delegate :name, to: :role, prefix: true

  Role::LIST.each do |role|
    define_method "#{role}?" do
      self.role_name == role
    end
  end

  def name
    "#{first_name} #{last_name}"
  end
end
