class Spree::Order < LegacyClient
  self.table_name = 'spree_orders'

  def self.completed
    where(state: 'complete')
  end
end
