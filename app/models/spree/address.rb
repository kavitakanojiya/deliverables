class Spree::Address < LegacyClient
  self.table_name = 'spree_addresses'

  belongs_to :country, class_name: "Spree::Country"
  belongs_to :state, class_name: "Spree::State"

  def full_address
    _address = [
      address1,
      address2,
      city,
      (state.try(:name) || ''),
      (country.try(:name) || ''),
      zipcode
    ].join(', ')
    if area.present?

    end
    _address
  end
end
