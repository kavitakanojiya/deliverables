module Accessible
  def orders
    case self.role_name
    when 'admin'
      OrderReference.all
    when 'carrier'
      # TBD: We might have another column to represent order assignment to carriers
      OrderReference.none
    when 'shopper'
      OrderReference.where(shopper: self).distinct
    when 'supervisor'
      # TBD
      OrderReference.none
    when 'vendor'
      OrderReference.joins(:order_items).where("order_items.vendor_id = :vendor_id", vendor_id: self.id).distinct
    end
  end
end
