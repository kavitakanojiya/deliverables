class OrderReference < ApplicationRecord
  include Accessible

  belongs_to :canceler, class_name: 'User'
  belongs_to :parent_order, class_name: 'OrderReference'
  belongs_to :shopper, class_name: 'User'
  has_many   :order_items

  scope :today, -> { where(delivery_date: Date.today) }

  store :user_details, accessors: [ :first_name, :last_name, :phone ]

  def shipment_address
  	_address = Spree::Address.find(shipment_address_id)
  	_address.full_address
  end
end
