class LegacyClient < ActiveRecord::Base
  establish_connection Settings.legacy_client
  self.abstract_class = true
end
