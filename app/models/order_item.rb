class OrderItem < ApplicationRecord
  # by default, all products will be ready
  # pending from SHOPPER
  # shipped from CARRIER
  # deleted if order_item not available
	STATES = ['deleted', 'pending', 'ready', 'shipped']

  belongs_to :order_reference
  belongs_to :vendor, class_name: 'User'
end
