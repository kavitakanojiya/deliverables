ActiveAdmin.register Spree::Order, as: 'All Orders' do
  menu if: proc { current_user.admin? || current_user.shopper? }

  actions :index

  controller do
    def scoped_collection
       Spree::Order.completed.page(params[:page]).per(10)
    end
  end

  filter :delivery_date
  filter :delivery_slot

  index do
    selectable_column
    id_column
    column :number
    column :item_total
    column :state
    column :email
    column :delivery_date
    column :delivery_slot
  end
end
