ActiveAdmin.register OrderReference do
  permit_params :additional_tax_total, :confirmation_delivered,
    :considered_risky, :delivery_date, :delivery_slot, :first_name,
    :included_tax_total, :item_total, :last_name, :payment_total,
    :phone, :promo_total, :shipment_total, :total

  actions :all, except: [:create, :new]

  controller do
    def scoped_collection
       current_user.orders.page(params[:page]).per(10)
    end
  end

  filter :delivery_date
  filter :delivery_slot
  filter :number
  filter :user_email

  form do |f|
    f.inputs "Order Reference: #{f.object.number}" do
      f.input :confirmation_delivered
      f.input :considered_risky
      f.input :delivery_date
      f.input :additional_tax_total
      f.input :adjustment_total
      f.input :included_tax_total
      f.input :item_total
      f.input :payment_total
      f.input :promo_total
      f.input :shipment_total
      f.input :total
      f.input :delivery_slot, as: :select, collection: LIST_OF_SLOTS_ADMIN, include_blank: false
      f.input :first_name
      f.input :last_name
      f.input :phone
    end
    f.actions
  end

  index do
    selectable_column
    id_column
    column :number
    column :total
    column :delivery_slot
    column :shipment_address_id
    column :state
    column :user_email
    actions
  end

  show do
    attributes_table do
      row :number
      row :total
      row(:delivery_slot) do |o_r|
        LIST_OF_SLOTS_ADMIN.to_h.key(o_r.delivery_slot)
      end
      row :shipment_address_id
      row :state
      row :user_email
      row :delivery_date
      row :first_name
      row :last_name
      row :phone
      row 'Order Items' do |o_r|
        table_for o_r.order_items do
          column :name
          column :price
          column :quantity
          column(:vendor) { |o_i| o_i.vendor.name }
          column :state
          column :barcode
        end
      end
    end
  end
end
