ActiveAdmin.register User do
  menu if: proc { current_user.admin? }

  permit_params :email, :password, :password_confirmation, :role_id

  index do
    selectable_column
    id_column
    column :name do |user|
      user.name
    end
    column :email
    column :current_sign_in_at
    column :sign_in_count
    column :created_at
    column :role
    actions
  end

  filter :email
  filter :role

  form do |f|
    role = f.object.role
    identifier = role ? role.name.capitalize : 'User'
    f.inputs "#{identifier} Details" do
      f.input :first_name
      f.input :last_name
      f.input :email
      f.input :password
      f.input :password_confirmation
      f.input :role
    end
    f.actions
  end
end
