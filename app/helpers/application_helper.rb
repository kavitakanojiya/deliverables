module ApplicationHelper
  def current_navigation(current, intended)
    current == intended ? 'active' : ''
  end
end
