class DeviseSessionsController < Devise::SessionsController
  def create
    super do |resource|
      respond_to do |format|
        format.html {
          respond_with resource, location: after_sign_in_path_for(resource) and return
        }
      end
    end
  end

  protected
    def after_sign_in_path_for(resource)
      orders_path
    end
end
