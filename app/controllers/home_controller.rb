class HomeController < ApplicationController
  layout 'authentication'

  def index
    if user_signed_in?
      redirect_to todays_order_references_path
    else
      redirect_to users_sign_in_path
    end
  end
end
