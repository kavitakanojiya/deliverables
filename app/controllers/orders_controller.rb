class OrdersController < ApplicationController
  layout 'deliverables'

  before_action :authenticate_user!
  before_action :authorize_user!

  def index
  	@orders = Spree::Order.completed
  end
end