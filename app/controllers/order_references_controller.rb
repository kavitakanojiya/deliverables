class OrderReferencesController < ApplicationController
  layout 'deliverables'

  before_action :authenticate_user!

  def todays
    @orders = current_user.orders.today
  end

  def index
    @orders = current_user.orders
  end
end