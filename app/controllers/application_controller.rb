class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  def authorize_user!
    unless (current_user.admin? || current_user.shopper?)
      flash[:error] = 'You are not authorize to access this page!'
      redirect_to root_path and return
    end
  end
end
