###### Setup: Starts Here ######
start postgres server: pg_ctl -D /usr/local/var/postgres -l /usr/local/var/postgres/server.log start
bundle install
rake db:create
To seed test data:
  - Skip `rake db:migrate`
  - psql -h localhost -U postgres -d deliverables_dev < deliverables_dev1
###### Setup: Ends Here ######



######### Rake list: Starts Here #########
`rake db:seed`
######### Rake list: Ends Here #########



######### Use-cases: Starts here #########
# Shopper:
Will pull order and assign order to Vendor
Only Shopper can set ready/pending flag to order_items
Shopper should be able to see all order_references that are assigned to them

# Vendor: #
Vendor should see only those order_references which has order_items assigned to them

# Carrier: TBD #

# Supervisor: TBD #
######### Use-cases: Ends here #########



######### New Pages: Starts here #########
All Orders: Visible to Admins + Shoppers
Order References: Visible to Admins + Shoppers
Users: Visible to Admins only
######### New Pages: Ends here #########



######### Implementation Details: Starts here #########
# All Orders #
- Reads from legacy(Grabmuch) database
- Retrieves only completed order

# Order References #
- Shoppers + Admins: Will see all order_references pulled into this system
- Vendors: Will see only those order_references whose children order_items are assigned to them

# Users #
- Only admins will have access this
- Admin can add Shoppers, Vendors and Carriers
######### Implementation: Ends here #########
