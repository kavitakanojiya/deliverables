# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160806171149) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "active_admin_comments", force: :cascade do |t|
    t.string   "namespace"
    t.text     "body"
    t.string   "resource_id",   null: false
    t.string   "resource_type", null: false
    t.string   "author_type"
    t.integer  "author_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id", using: :btree
    t.index ["namespace"], name: "index_active_admin_comments_on_namespace", using: :btree
    t.index ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id", using: :btree
  end

  create_table "order_items", force: :cascade do |t|
    t.decimal "price"
    t.integer "quantity"
    t.integer "order_reference_id"
    t.integer "vendor_id"
    t.string  "state"
    t.text    "barcode"
    t.text    "name"
    t.index ["order_reference_id"], name: "index_order_items_on_order_reference_id", using: :btree
    t.index ["vendor_id"], name: "index_order_items_on_vendor_id", using: :btree
  end

  create_table "order_references", force: :cascade do |t|
    t.boolean  "confirmation_delivered",                                     default: false
    t.boolean  "considered_risky",                                           default: false
    t.date     "invoice_date"
    t.datetime "canceled_at"
    t.datetime "completed_at"
    t.datetime "delivery_date"
    t.decimal  "additional_tax_total",              precision: 10, scale: 2, default: "0.0"
    t.decimal  "adjustment_total",                  precision: 10, scale: 2, default: "0.0", null: false
    t.decimal  "included_tax_total",                precision: 10, scale: 2, default: "0.0", null: false
    t.decimal  "item_total",                        precision: 10, scale: 2, default: "0.0", null: false
    t.decimal  "payment_total",                     precision: 10, scale: 2, default: "0.0"
    t.decimal  "promo_total",                       precision: 10, scale: 2, default: "0.0"
    t.decimal  "shipment_total",                    precision: 10, scale: 2, default: "0.0", null: false
    t.decimal  "total",                             precision: 10, scale: 2, default: "0.0", null: false
    t.integer  "delivery_slot"
    t.integer  "external_oid"
    t.integer  "invoice_number"
    t.integer  "shipment_address_id"
    t.integer  "canceler_id"
    t.integer  "parent_order_id"
    t.integer  "shopper_id"
    t.string   "shipment_state"
    t.string   "number",                 limit: 32
    t.string   "state"
    t.string   "user_email"
    t.text     "user_details"
    t.datetime "created_at",                                                                 null: false
    t.datetime "updated_at",                                                                 null: false
    t.index ["canceler_id"], name: "index_order_references_on_canceler_id", using: :btree
    t.index ["parent_order_id"], name: "index_order_references_on_parent_order_id", using: :btree
    t.index ["shopper_id"], name: "index_order_references_on_shopper_id", using: :btree
  end

  create_table "roles", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "first_name"
    t.string   "last_name"
    t.string   "phone"
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.integer  "role_id"
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
    t.index ["role_id"], name: "index_users_on_role_id", using: :btree
  end

end
