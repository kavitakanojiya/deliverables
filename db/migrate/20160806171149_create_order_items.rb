class CreateOrderItems < ActiveRecord::Migration[5.0]
  def change
    create_table :order_items do |t|
      t.decimal :price

      t.integer :quantity

      t.references :order_reference
      t.references :vendor

      t.string  :state

      t.text    :barcode
      t.text    :name
    end
  end
end
