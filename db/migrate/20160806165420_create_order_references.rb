class CreateOrderReferences < ActiveRecord::Migration[5.0]
  def change
    create_table :order_references do |t|
      t.boolean  :confirmation_delivered, default: false
      t.boolean  :considered_risky,       default: false

      t.date     :invoice_date
      t.datetime :canceled_at
      t.datetime :completed_at
      t.datetime :delivery_date

      t.decimal  :additional_tax_total,   precision: 10, scale: 2, default: 0.0
      t.decimal  :adjustment_total,       precision: 10, scale: 2, default: 0.0,     null: false
      t.decimal  :included_tax_total,     precision: 10, scale: 2, default: 0.0,     null: false
      t.decimal  :item_total,             precision: 10, scale: 2, default: 0.0,     null: false
      t.decimal  :payment_total,          precision: 10, scale: 2, default: 0.0
      t.decimal  :promo_total,            precision: 10, scale: 2, default: 0.0
      t.decimal  :shipment_total,         precision: 10, scale: 2, default: 0.0,     null: false
      t.decimal  :total,                  precision: 10, scale: 2, default: 0.0,     null: false

      t.integer  :delivery_slot
      t.integer  :external_oid
      t.integer  :invoice_number
      t.integer  :shipment_address_id

      t.references :canceler
      t.references :parent_order
      t.references :shopper

      t.string   :shipment_state
      t.string   :number,                 limit: 32
      t.string   :state
      t.string   :user_email

      t.text     :user_details

      t.timestamps null: false
    end
  end
end
